package com.MapGen;

public class MapTile {

    // Tile content codes (depend on Obsidio and thus are NOT TO BE TOUCHED)
    private static final int emptyTile = 0,
                             tallRock = 1,
                             smallRock = 2,
                             leftWind = 3,
                             rightWind = 4,
                             upWind = 5,
                             downWind = 6,
                             poolTopLeft = 7,
                             poolTopRight = 8,
                             poolBottomLeft = 9,
                             poolBottomRight = 10,
                             flag1P = 11,
                             flag2P = 12,
                             flag3P = 13;

    // Utility enums (for later definitions)

    public enum poolCorner{topLeft, topRight, bottomLeft, bottomRight}
    public enum windDirection{left, right, up, down}

    // Object fields

    private int tileContent;
    private float poolChance,
                  windChance,
                  rockChance;
    private windDirection windTendency;


    // Accessors
    public int getTileContent(){
        return this.tileContent;
    }
    public float getPoolChance() {
        return this.poolChance;
    }
    public float getWindChance() {
        return this.windChance;
    }
    public float getRockChance(){
        return this.rockChance;
    }
    public windDirection getWindTendency() {
        return this.windTendency;
    }
    public boolean isTileEmpty(){
        return this.tileContent == emptyTile;
    }

    public boolean isWindTendencyHorizontal(){
        return this.windTendency == windDirection.left || this.windTendency == windDirection.right;
    }
    public boolean isWindTendencyVertical(){
        return this.windTendency == windDirection.down || this.windTendency == windDirection.up;
    }

    // Content modifiers

    public void makePointsInTile(int points){
        switch (points){
            case 0:
                break;
            case 1:
                this.tileContent = flag1P;
                break;
            case 2:
                this.tileContent = flag2P;
                break;
            default:
                this.tileContent = flag3P;
                break;
        }
    }

    public void makeRocksInTile(boolean isRockTall){
        this.tileContent = isRockTall ? tallRock : smallRock;
    }
    public void increaseRockChancesInTile(float amount){
        this.rockChance += amount;
    }

    public void makePoolCornerInTile(poolCorner whichCorner) {
        switch (whichCorner) {
            case topLeft:
                this.tileContent = poolTopLeft;
                break;
            case topRight:
                this.tileContent = poolTopRight;
                break;
            case bottomLeft:
                this.tileContent = poolBottomLeft;
                break;
            case bottomRight:
                this.tileContent = poolBottomRight;
                break;
        }
    }
    public void increaseWindChancesInTile(float amount) {
        this.windChance += amount;
    }
    public void increasePoolChancesInTile(float amount){
        this.poolChance += amount;
    }

    public void makeWindInTile(){
        switch (this.windTendency){
            case right:
                this.tileContent = rightWind;
                break;
            case left:
                this.tileContent = leftWind;
                break;
            case up:
                this.tileContent = upWind;
                break;
            case down:
                this.tileContent = downWind;
                break;
        }
    }

    public void setWindTendency(windDirection tendency){
        this.windTendency = tendency;
    }

    // Utility methods
    private static windDirection pickRandomDirection(){
        switch (MapGen.randomIntFromRange(0, 4)){
            case 0:
                return windDirection.down;
            case 1:
                return windDirection.up;
            case 2:
                return windDirection.left;
            default:
                return windDirection.right;
        }
    }




    // Class constructor

    public MapTile(float baselinePoolOdds, float baselineWindOdds, float baselineRockOdds){
        this.tileContent = emptyTile;
        this.poolChance = baselinePoolOdds;
        this.windChance = baselineWindOdds;
        this.rockChance = baselineRockOdds;
        this.windTendency = pickRandomDirection();
    }
}
