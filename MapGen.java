/*************************************************
 Random map generator for Obsidio               *
 Created by Lucas del Pino                      *
 Feel free to use the program as-is,            *
 or to modify as you wish (but do give credit)  *
 *************************************************/

package com.MapGen;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class MapGen {

    // Constraining constants

    private static final int boardLength = 36,
                             boardWidth = 20,
                             safeZoneSize = 3;

    private static final float flagChance = 0.125f,
                               rockChance = 0.01f,
                               rockChanceBump = 0.025f,
                               windChance = 0.02f,
                               windChanceBump = 0.07f,
                               poolChance = 0.003f,
                               poolChanceBump = 0.02f;

    // Random methods

    private static Random rand = new Random(System.currentTimeMillis());

    public static boolean rollRandomChance(float test){
        return rand.nextFloat() < test;
    }

    public static int randomIntFromRange(int lowerBound, int upperBound){
        int range = upperBound - lowerBound;
        return lowerBound + rand.nextInt(range);
    }

    // Logical methods

    private static boolean doesTileExist(int tileX, int tileY){
        return ((tileX >= 0) && (tileX < boardWidth)) &&
                ((tileY >= (safeZoneSize - 1)) && (tileY < (boardLength - safeZoneSize)));
    }

    private static boolean enoughRoomForPool(int tileX, int tileY){
        return (doesTileExist(tileX, tileY) && doesTileExist(tileX + 1, tileY)) &&
                (doesTileExist(tileX, tileY + 1) && doesTileExist(tileX + 1, tileY + 1));
    }

    // I/O utilities

    private static String getMapString(MapTile[][] map){
        String result = "";
        for(int i = 0; i < boardLength; i++){
            for(int j = 0; j < boardWidth; j++){
                result = result + map[j][i].getTileContent() + ",";
            }
            result = result + "\n";
        }
        return result;
    }

    private static int askUserForInt(String message, int defaultValue){
        int result;
        while(true){
            Scanner scn = new Scanner(System.in);
            System.out.println(message);
            System.out.println("Please input a positive integer, or \"d\" to use the default value of " + defaultValue + ": ");
            if (scn.hasNextInt()){
                result = scn.nextInt();
                if (result <= 0){
                    System.out.println("Input equal or lesser than zero. Try again");
                } else break;
            } else if (scn.hasNext() && scn.next().matches("d")){
                result = defaultValue;
                break;
            } else{
                System.out.println("Invalid input. Try again");
            }
            scn.close();
        }
        return result;
    }

    private static void printMapIntoFile(String mapString) throws IOException {
        FileOutputStream out = null;
        String filename;
        while (true){
            Scanner scn = new Scanner(System.in);
            System.out.println("Please enter the name of the file: ");
            if (scn.hasNext()){
                filename = scn.next();
                break;
            } else{
                System.out.println("Error! no entry");
            }
            scn.close();
        }
        try{
            out = new FileOutputStream(filename);
            for(char tile : mapString.toCharArray()){
                out.write(tile);
            }
        }finally {
            if (out != null){
                out.close();
            }
        }

    }

    // Map building functions

    private static void populateMap(MapTile[][] map){
        for(int i = 0; i < boardWidth; i++){
            for(int j = 0; j < boardLength; j++){
                map[i][j] = new MapTile(poolChance, windChance, rockChance);
            }
        }
    }

    private static void makeCluster(MapTile[][] map, int originX, int originY, int radius){
        for(int i = -radius; i < radius; i++){
            for(int j = -radius; j < radius; j++){
                if(doesTileExist(originX + i, originY + j) && rollRandomChance(flagChance)){
                    map[originX + i][originY + j].makePointsInTile(randomIntFromRange(1,4));
                }
            }
        }
    }

    private static void decideClusters(MapTile[][] map){
        int clusterAmount = askUserForInt("How many clusters will be placed?", 2);
        int clusterRadius = askUserForInt("How large will be the cluster radius?", 3);
        for(int i = 0; i < clusterAmount; i++){
            makeCluster(map,
                        randomIntFromRange(0, boardWidth),
                        randomIntFromRange(safeZoneSize - 1, boardLength - safeZoneSize),
                        clusterRadius);
        }
    }

    private static void makePool(MapTile[][] map, int topLeftX, int topLeftY){
        if(map[topLeftX][topLeftY].isTileEmpty() && map[topLeftX+1][topLeftY].isTileEmpty() &&
                map[topLeftX][topLeftY+1].isTileEmpty() && map[topLeftX+1][topLeftY+1].isTileEmpty()) {
            map[topLeftX][topLeftY].makePoolCornerInTile(MapTile.poolCorner.topLeft);
            map[topLeftX+1][topLeftY].makePoolCornerInTile(MapTile.poolCorner.topRight);
            map[topLeftX][topLeftY+1].makePoolCornerInTile(MapTile.poolCorner.bottomLeft);
            map[topLeftX+1][topLeftY+1].makePoolCornerInTile(MapTile.poolCorner.bottomRight);
        }
    }

    private static void handleEnvironment(MapTile[][] map){
        int iterations = askUserForInt("How many passes to add elements to the map?", 10);
        for (int n = 0; n < iterations; n++){
            for (int i = 0; i < boardWidth; i++){
                for (int j = safeZoneSize; j < boardLength - safeZoneSize; j++){
                    if(map[i][j].isTileEmpty()){
                        // Rocks
                        if(rollRandomChance(map[i][j].getRockChance())){
                            map[i][j].makeRocksInTile(rand.nextBoolean());
                            for(int p = -1; p <= 1; p++){
                                for(int q = -1; q <= 1; q++){
                                    if(doesTileExist(i+p, j+q)){
                                        map[i+p][j+q].increaseRockChancesInTile(rockChanceBump);
                                    }
                                }
                            }
                            continue;
                        }
                        // Pools
                        if(rollRandomChance(map[i][j].getPoolChance()) && enoughRoomForPool(i, j)){
                            makePool(map, i, j);
                            // Influence
                            for(int p = -1; p <= 2; p++){
                                for(int q = -1; q <= 2; q++){
                                    if(doesTileExist(i+p, j+q)){
                                        map[i+p][j+q].increasePoolChancesInTile(poolChanceBump);
                                    }
                                }
                            }
                            continue;
                        }
                        // Winds
                        if(rollRandomChance(map[i][j].getWindChance())){
                            map[i][j].makeWindInTile();
                            // Influence
                            for(int k = -1; k <= 1; k++){
                                // Assigns the tiles to alter based on whether the current is horizontal or vertical
                                int influenceX = (map[i][j].isWindTendencyHorizontal() ? i + k : i),
                                    influenceY = (map[i][j].isWindTendencyVertical() ? j + k : j);
                                if(doesTileExist(influenceX, influenceY)){
                                    map[influenceX][influenceY].increaseWindChancesInTile(windChanceBump);
                                    map[influenceX][influenceY].setWindTendency(map[i][j].getWindTendency());
                                }

                            }
                            continue;
                        }
                    }
                }

            }
        }
    }

    private static String makeMap(){

        MapTile[][] myMap = new MapTile[boardWidth][boardLength];
        populateMap(myMap); // Initialize the array
        decideClusters(myMap); // Place points
        handleEnvironment(myMap); // Sort out all the environment of the board


        return getMapString(myMap);
    }

    public static void main(String[] args) {
        String myMap = makeMap();
        System.out.println(myMap);
        try {
            printMapIntoFile(myMap);
        } catch (IOException ex){
            System.out.println("Failed to save file");
        }
    }
}
